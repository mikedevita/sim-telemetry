import React, { Component } from 'react';
import Light from './Light';

export default class LightPanel extends Component {
    render() {
        const {
            lights,
        } = this.props
        
        return (<ul className='lights'>
            {lights &&
                Object.keys(lights).map((light, i) => (<Light key={i} name={lights[light].label} isOn={lights[light].isOn} />))
            }
        </ul>);
    }
}