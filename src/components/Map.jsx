import React, { Component } from 'react'
import { GoogleMap, useJsApiLoader } from '@react-google-maps/api'
import Config from '../config'
 
function MapComponent ({zoom = 10, width, height, lat = Config.coordinates.defaultLat, lng = Config.coordinates.defaultLng}) {
    const { isLoaded } = useJsApiLoader({
      id: 'google-map-script',
      googleMapsApiKey: "AIzaSyA_2ihcs4dUgx32-LvBjL2LUmYRnEAO6kg"
    })
  
    const [map, setMap] = React.useState(null)
  
    const onLoad = React.useCallback(function callback(map) {
      const bounds = new window.google.maps.LatLngBounds();
      map.fitBounds(bounds);
      setMap(map)
    }, [])
  
    const onUnmount = React.useCallback(function callback(map) {
      setMap(null)
    }, [])
  
    return isLoaded ? (
        <GoogleMap
          mapContainerStyle={{ width: width, height: height }}
          center={{ lat, lng }}
          zoom={zoom}
          onLoad={onLoad}
          onUnmount={onUnmount}
        >
          { /* Child components, such as markers, info windows, etc. */ }
          <></>
        </GoogleMap>
    ) : <></>
  }
  
export default React.memo(MapComponent)

