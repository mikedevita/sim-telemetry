import React, { Component } from 'react'
import { MapContainer, TileLayer, Marker } from 'react-leaflet'
import Config from '../config'

export class MapComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            center: {
                lat: Config.map.defaultLat,
                lng: Config.map.defaultLng,
            },
            zoom: 13,
            lat: null,
            lng: null,
            scrollWheelZoom: true,
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {
            ...state,
            lat: props.lat,
            lng: props.lng,
            center: {
                lat: props.lat,
                lng: props.lng,
            },
        }
    }
    
    render() {
        const {
            center,
            zoom,
            scrollWheelZoom,
            lat, 
            lng,
        } = this.state

        return (<div className='map'>
            <MapContainer center={center} zoom={zoom} scrollWheelZoom={scrollWheelZoom}>
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={{ lat: lat, lng: lng }}>
                    
                </Marker>
            </MapContainer>
        </div>);
    }

}

export default MapComponent