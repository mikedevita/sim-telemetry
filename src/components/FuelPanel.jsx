import React, { Component } from 'react';

export default class FuelPanel extends Component {
    render() {
        const {
            leftFuelTank,
            rightFuelTank,
        } = this.props;
        
        return (<div id='FuelPanelComponent' className='fuelTanks'>
            {leftFuelTank &&
            <div id='leftTank' className='fuelTank'>
                <span className='fuelTankTitle'>Left</span>
                <div className='fuelTankFill'/>
            </div>
            }
            {rightFuelTank &&
            <div id='rightTank' className='fuelTank'>
                <span className='fuelTankTitle'>Right</span>
                <div className='fuelTankFill'/>
            </div>
            }
        </div>)
    }
}