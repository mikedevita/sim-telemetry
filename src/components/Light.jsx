import React, { Component } from 'react';
import classNames from 'classnames';

export default class Light extends Component {
    render() {
        const {
            name,
            isOn,
        } = this.props;
        
        return (<li className={classNames('light', { 'light-on': isOn })}>
            <p>{name}</p>
            <div className="lightIndicator"/>
        </li>)
    }
}