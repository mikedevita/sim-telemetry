import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import SimService from './SimService';

function App() {
  return (
    <div className="App">
      <SimService />
    </div>
  );
}

export default App;
