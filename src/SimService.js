import React, { Component } from 'react';
import classNames from 'classnames';
import { Container, Row, Col, ButtonGroup } from 'react-bootstrap';
import _ from 'lodash';
import Config from './config';
import LightPanel from './components/LightPanel';
import FuelPanel from './components/FuelPanel';
import Map from './components/GoogleMap';

const ButtonToggle = ({ isConnected, handleDisconnect, handleConnect }) => {
    return (isConnected)
        ? <button className={classNames('btn btn-lg', {'btn-secondary': !isConnected, 'btn-danger': isConnected })} onClick={handleDisconnect}>Disconnect</button>
        : <button className={classNames('btn btn-lg', {'btn-primary': !isConnected, 'btn-danger': isConnected })} onClick={handleConnect}>Connect</button>
}

class SimServiceComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            debugMode: false,
            isConnected: false,
            isConnecting: false,
            isReceiving: false,
            offsetsDeclared: false,
            hasErrors: false,
            errors: null,
            forceDisconnect: false,
            showMap: false,
            updateInterval: 100,
            offsetName: 'myOffsets',
            offsets: [
                { enabled: true, name: 'altitude', address: 0x0570, type: 'int', size: 8 }, // applied calculation to response - (altitude / (65535 * 65535) * 3.28084).toFixed(0)
                { enabled: true, name: 'heading', address: 0x0580, type: 'uint', size: 4 }, // applied calculation to response - (heading * 360 / (65536 * 65536)).toFixed(0)
                { enabled: true, name: 'tas', address: 0x02B8, type: 'int', size: 4 }, // applied calculation to response - (airspeed / 128).toFixed(0)
                { enabled: true, name: 'ias', address: 0x02BC, type: 'int', size: 4 }, // applied calculation to response - (airspeed / 128).toFixed(0)
                { enabled: true, name: 'destETA', address: 0x619C, type: 'int', size: 4 },
                { enabled: true, name: 'destETE', address: 0x6198, type: 'int', size: 4 },
                { enabled: true, name: 'localTime', address: 0x0238, type: 'int', size: 1 },
                { enabled: true, name: 'localTimeHH', address: 0x0238, type: 'int', size: 1 },
                { enabled: true, name: 'localTimeMM', address: 0x0239, type: 'int', size: 1 },
                { enabled: true, name: 'localTimeSS', address: 0x023A, type: 'int', size: 1 },
                { enabled: true, name: 'avionicsMaster', address: 0x2E80, type: 'uint', size: 4 },
                { enabled: true, name: 'aircraftName', address: 0x3D00, type: 'string', size: 256 },
                { enabled: true, name: 'lights', address: 0x0D0C, type: 'bits', size: 2 },
                { enabled: true, name: 'playerLon', address: 0x0568, type: 'lon', size: 8 },
                { enabled: true, name: 'playerLat', address: 0x0560, type: 'lat', size: 8 },
                { enabled: true, name: 'todIndicator', address: 0x115E, type: 'int', size: 1 },
                { enabled: true, name: 'leftFlapsPosition', address: 0x0BE0, type: 'int', size: 1 },
                { enabled: true, name: 'rightFlapsPosition', address: 0x0BE4, type: 'int', size: 1 },
                { enabled: true, name: 'leftSpoilerPosition', address: 0x0BD4, type: 'int', size: 4 },
                { enabled: true, name: 'rightSpoilerPosition', address: 0x0BD8, type: 'int', size: 4 },
                { enabled: true, name: 'parkingBrake', address: 0x0BC8, type: 'int', size: 2 },
                { enabled: true, name: 'noseGearPosition', address: 0x0BEC, type: 'int', size: 4 },
                { enabled: true, name: 'leftGearPosition', address: 0x0BF4, type: 'int', size: 4 },
                { enabled: true, name: 'rightGearPosition', address: 0x0BF0, type: 'int', size: 4 },
                { enabled: true, name: 'leftBrake', address: 0x0BF0, type: 'int', size: 4 },
                { enabled: true, name: 'rightBrake', address: 0x0BF0, type: 'int', size: 4 },
                { enabled: true, name: 'isBraking', address: 0x0BCA, type: 'int', size: 2 },
                { enabled: true, name: 'destination', address: 0x6137, type: 'string', size: 5 },
            ],
            data: {
                altitude: null,
                heading: null,
                aircraftName: null,
                avionicsMaster: null,
                lights: {
                    nav: {
                        label: 'NAV',
                        isOn: false,
                    },
                    taxi: {
                        label: 'TAXI',
                        isOn: false,
                    },
                    strobe: {
                        label: 'STROBE',
                        isOn: false,
                    },
                    landing: {
                        label: 'LNDG',
                        isOn: false,
                    },
                    wing: {
                        label: 'WING',
                        isOn: false,
                    },
                    antiCollision: {
                        label: 'COLL',
                        isOn: false,
                    },
                    tailFlood: {
                        label: 'TAIL',
                        isOn: false,
                    },
                    recog: {
                        label: 'RECOG',
                        isOn: false,
                    },
                },
                fuelTanks: [],
                weight: null,
                coordinates: {
                    lat: null,
                    lon: null,
                },
                ias: null,
                tas: null,
                destETA: null,
                destETE: null,
                localTime: null,
                localTimeHH: null,
                localTimeMM: null,
                localTimeSS: null,
                arrivalTime: null,
                todIndicator: null,
                destination: null,
                flapsPosition: {
                    leftFlaps: null,
                    rightFlaps: null,
                },
                parkingBrake: null,
                spoilerPosition: {
                    leftSpoiler: null,
                    rightSpoiler: null,
                },
                landingGearPositions: {
                    nose: null,
                    left: null,
                    right: null,
                },
                brake: {
                    left: null,
                    right: null,
                },
                isBraking: null,
            }
        };
        this.ws = null
    }

    readSim = () => {
        this.declareOffsets()
        .then(() => {
            this.readOffsets()
        })

        // this.readPayload()
    }

    readOffsets = () => {
        const {
            offsetsDeclared,
            offsetName,
            updateInterval,
            changesOnly,
        } = this.state

        if (offsetsDeclared) {
            this.sendMessage({
                command: 'offsets.read',
                name: offsetName,
                changesOnly,
                interval: updateInterval,
            })
            .then(() => {
                this.setState({
                    isReceiving: true
                })
            })
        }
    }

    toggleDebug = () => {
        this.setState({
            debugMode: !this.state.debugMode
        })
    }

    declareOffsets = () => {
        const {
            offsets
        } = this.state

        return new Promise ((resolve, reject) => {
            this.sendMessage({
                command: 'offsets.declare',
                name: 'myOffsets',
                offsets: offsets.filter(o => o.enabled),
            })
            .then((response) => {
                if (this.state.debugMode) {
                    console.log('myOffsetsDeclared()', response)
                }
                this.setState({
                    offsetsDeclared: true,
                })
                resolve()
            })
        })
    }

    sendMessage = (request) => {
        return new Promise((resolve, reject) => {
            if (!request) return reject('no message supplied')
            if ((this.ws !== null && this.ws.readyState === 1)) {
                const response = this.ws.send(JSON.stringify(request))
                return resolve(response)
            } else {
                return reject('not in connected readyState')
            }
        })
    }

    disconnect = () => {
        if (this.ws !== null) {
            this.ws.close();
        }

        this.setState({
            isConnected: false,
        });
    }

    calculateAltitude = (altitude) => {
        return (altitude / (65535 * 65535) * 3.28084).toFixed(0);
    }

    calculateHeading = (heading) => {
        return (heading * 360 / (0.65536 * 65536)).toFixed(0);
    }

    calculateAirSpeed = (airspeed) => {
        return (airspeed / 128).toFixed(0);
    }

    parseOffsets = (data) => {
        if (this.state.debugMode) {
            console.log('readOffsets()', data)
        }
        const newData = _.merge(this.state.data, {
            altitude: (data.altitude) ? this.calculateAltitude(data.altitude) : data.altitude,
            heading: (data.heading) ? this.calculateHeading(data.heading) : data.heading,
            lights: {
                nav: {
                    isOn: (data.lights[0] !== this.state.data.lights.nav.isOn) ? data.lights[0] : this.state.data.lights.nav.isOn,
                },
                taxi: {
                    isOn: (data.lights[3] !== this.state.data.lights.taxi.isOn) ? data.lights[3] : this.state.data.lights.taxi.isOn,
                },
                strobe: {
                    isOn: (data.lights[4] !== this.state.data.lights.strobe.isOn) ? data.lights[4] : this.state.data.lights.strobe.isOn,
                },
                landing: {
                    isOn: (data.lights[2] !== this.state.data.lights.landing.isOn) ? data.lights[2] : this.state.data.lights.landing.isOn,
                },
                recog: {
                    isOn: (data.lights[6] !== this.state.data.lights.recog.isOn) ? data.lights[6] : this.state.data.lights.recog.isOn,
                },
                wing: {
                    isOn: (data.lights[7] !== this.state.data.lights.wing.isOn) ? data.lights[7] : this.state.data.lights.wing.isOn,
                },
                antiCollision: {
                    isOn: (data.lights[1] !== this.state.data.lights.antiCollision.isOn) ? data.lights[1] : this.state.data.lights.antiCollision.isOn,
                },
                tailFlood: {
                    isOn: (data.lights[8] !== this.state.data.lights.tailFlood.isOn) ? data.lights[8] : this.state.data.lights.tailFlood.isOn,
                },
            },
            coordinates: {
                lat: data.playerLon,
                lon: data.playerLat,
            },
            ias: this.calculateAirSpeed(data.ias),
            tas: this.calculateAirSpeed(data.tas),
            destETA: data.destETA,
            destETE: data.destETE,
            localTime: data.localTime,
            arrivalTime: data.localTime + data.destETE,
            localTimeHH: data.localTimeHH,
            localTimeMM: data.localTimeMM,
            localTimeSS: data.localTimeSS,
            todIndicator: data.todIndicator,
            destination: data.destination,
            flapsPosition: {
                leftFlaps: data.leftFlapsPosition,
                rightFlaps: data.rightFlapsPosition,
            },
            spoilerPosition: {
                leftSpoiler: data.leftSpoilerPosition,
                rightSpoiler: data.rightSpoilerPosition,
            },
            landingGearPosition: {
                nose: data.noseGearPosition,
                left: data.leftGearPosition,
                right: data.rightGearPosition,
            },
            brake: {
                left: data.leftBrake,
                right: data.rightBrake,
            },
            parkingBrake: (data.parkingBrake === 32768) ? true : false,
            isBraking: (data.isBraking > 0) ? true : false,

            // leftFlapsPosition
            // rightFlapsPosition
            // leftSpoilerPosition
            // rightSpoilerPosition
            // parkingBrake
            // noseGearPosition
            // leftGearPosition
            // rightGearPosition
            // leftBrake
            // rightBrake
            // isBraking
            // destination
        });
        
        this.setState({
            data: {
                ...newData
            },
        });

    }

    /**
     * stubbed
     */
    readPayload = (data) => {
        // console.log('readPayload()', data);
        const fuelTanks = data.fuelTanks.filter(v => v.isPresent);

        const newData = _.merge(this.state.data, {
            fuelTanks: [
                ...fuelTanks,
            ],
            totalFuelVolume: data.totalFuelVolume,
            totalFuelCapacityWeight: data.totalFuelCapacityWeight,
            totalFuelCapacityVolume: data.totalFuelCapacityVolume,
            totalFuelPercent: data.totalFuelPercent,
            weights: {
                grossWeight: data.grossWeight.toFixed(2),
                maxGrossWeight: data.maxGrossWeight.toFixed(2),
                emptyWeight: data.emptyWeight.toFixed(2),
                totalPayloadWeight: data.totalPayloadWeight.toFixed(2),
                totalFuelWeight: data.totalFuelWeight.toFixed(2),
            },
            payloads: data.payloadStations.filter(v => (v.weight > 0)).map((v, k) => ({
                    weight: v.weight.toFixed(2)
            })),
            weightUnit: data.weightUnit,
            volumeUnit: data.volumeUnit,
            lengthUnit: data.lengthUnit,
        });
    }
    

    connect = () => {
        return new Promise((resolve, reject) => {
            if (this.ws === null) {
                this.setState({
                    isConnecting: true,
                    isConnected: false,
                });

                this.ws = new WebSocket(Config.ws.url, Config.ws.channel)

                this.ws.onopen = () => {
                    this.setState({
                        isConnecting: false,
                        isConnected: (this.ws !== null),
                    })

                    resolve(this.ws)
                }

                this.ws.onclose = () => {
                    this.setState({
                        isConnected: false,
                        isConnecting: false,
                    });
    
                    // clear the WebSocket so we can try again
                    this.ws = null;
                }

                this.ws.onerror = (err) => {
                    this.setState({
                        isConnected: false,
                        isConnecting: false,
                        hasError: true,
                    })

                    reject(err)
                }

                this.ws.onmessage = (msg) => {
                    const response = JSON.parse(msg.data);

                    if (response.success) {
                        switch (response.command) {
                            case 'payload.read':
                                this.readPayload(response.data);
                            break;
                            case 'offsets.read':
                                this.parseOffsets(response.data);
                            break;
                            default:
                            break;
                        }
                    }
                }
            }
        })
    }


    componentDidMount() {
        if (Config.ws.autoConnect) {
            this.connect();

            setTimeout(() => {
                this.readSim();
            }, 300);
        }

        if (Config.ws.autoReconnect && !this.state.forceDisconnect) {
            setInterval(() => {
                if (!this.state.forceDisconnect && (this.ws === null || this.ws.readyState !== 1)) {
                    // console.log('disconnected', this);
                    // console.log('attempting reconnection');
                    this.connect();
                }
            }, Config.ws.ReconnectInterval);
        }
    }

    parseTod = (tod) => {
        let timeOfDay = ''
        switch (tod) {
            case 0:
                timeOfDay = 'Dawn'
            break;
            case 1:
                timeOfDay = 'Day'
            break;
            case 2:
                timeOfDay = 'Dusk'
            break;
            case 3:
                timeOfDay = 'Night'
            break;
            default:
            break;
        }

        return timeOfDay
    }

    render() { 
        const {
            isConnected,
            isConnecting,
            isReceiving,
            hasErrors,
            errors,
            data,
            showMap,
            debugMode,
        } = this.state;

        return (<div id='SimServiceComponent' className={classNames({ 'connected': isConnected, 'disconnected': (!isConnected && !isConnecting), 'connecting': isConnecting })}>
            <Container>
                {(hasErrors && errors.length > 0) &&
                    <Row>
                        <Col>
                            <ul>
                                {errors.map((error, k) => (<li key={k}>{error.msg}</li>))}
                            </ul>
                        </Col>
                    </Row>
                }
                {(isConnected && isReceiving) &&
                <>
                    <Row>
                        <Col>
                            <h3>{data.aircraftName}</h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ul>
                                <li><strong>Current Altitude:</strong> {data.altitude}</li>
                                <li><strong>IAS:</strong> {data.ias}</li>
                                <li><strong>TAS:</strong> {data.tas}</li>
                                <li><strong>ETA:</strong> {data.destETA}</li>
                                <li><strong>ETE:</strong> {data.destETE}</li>
                                <li><strong>TOD Indicator:</strong> {this.parseTod(data.todIndicator)}</li>
                                <li><strong>Local Time:</strong> {`${data.localTimeHH}:${data.localTimeMM}:${data.localTimeSS}`}</li>
                                <li><strong>Arrival Time:</strong> {data.arrivalTime}</li>
                                <li><strong>Current Heading:</strong> {data.heading}</li>
                                <li><strong>Avionics Master:</strong> {(data.avionicsMaster) ? 'ON': 'OFF'}</li>
                            </ul>
                        </Col>
                    </Row>
                        <Row>
                            <Col>
                                <h4>Lights</h4>
                                <LightPanel lights={data.lights} />
                            </Col>               
                        </Row>
                    <Row>
                        <Col>
                            <hr />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <h4>Fuel</h4>
                        </Col>
                    </Row>
                    {(data.fuelTanks.length > 0) && 
                    <Row>
                        <Col>
                            <FuelPanel leftFuelTank={data.fuelTanks[0]} rightFuelTank={data.fuelTanks[1]} />
                        </Col>
                    </Row>
                    }
                    
                    {(showMap && (data.coordinates.lat && data.coordinates.lon)) && 
                        <Row>
                            <Col>
                               <Map
                                    lat={data.coordinates.lat}
                                    lng={data.coordinates.lon}
                                />
                            </Col>
                        </Row>
                    }
                </>
                }

                <Row>
                    <Col>
                        <ButtonGroup>
                            <button className={classNames('btn btn-lg', {'btn-secondary': !isConnected, 'btn-primary': isConnected })} onClick={this.readSim} disabled={!isConnected}>Read Sim Details</button>
                            <button className={classNames('btn btn-lg', {'btn-warning': debugMode, 'btn-disabled': !debugMode })} onClick={this.toggleDebug}>Debug Mode</button>
                            <ButtonToggle
                                isConnected={isConnected}
                                isConnecting={isConnecting}
                                handleDisconnect={this.disconnect}
                                handleConnect={this.connect}
                            />
                        </ButtonGroup>
                    </Col>
                </Row>
            </Container>
        </div>);
    }
}
 
export default SimServiceComponent;